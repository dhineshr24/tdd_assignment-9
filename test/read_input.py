class ReadInput:

    def get_blog_input(self):
        blog_file = open("blogs","r")
        blogs = blog_file.readlines()
        blog_file.close()
        return blogs

    def get_post_input(self):
        post_file = open("posts", "r")
        posts = post_file.readlines()
        post_file.close()
        return posts